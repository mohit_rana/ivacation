//
//  ViewController.m
//  button
//
//  Created by Ankit on 4/1/16.
//  Copyright (c) 2016 Ankit. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self
               action:@selector(pressed)
     forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:@"Show View" forState:UIControlStateNormal];
    button.frame = CGRectMake(80.0, 210.0, 160.0, 40.0);
    button.backgroundColor=[UIColor blackColor];
    [self.view addSubview:button];
    // Do any additional setup after loading the view, typically from a nib.
}
-(void)pressed{
    NSLog(@"button is presed");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
