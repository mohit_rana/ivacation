//
//  AppDelegate.h
//  button
//
//  Created by Ankit on 4/1/16.
//  Copyright (c) 2016 Ankit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

