//
//  main.m
//  button
//
//  Created by Ankit on 4/1/16.
//  Copyright (c) 2016 Ankit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
